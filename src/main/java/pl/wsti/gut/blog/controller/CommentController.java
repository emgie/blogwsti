package pl.wsti.gut.blog.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pl.wsti.gut.blog.model.Comment;
import pl.wsti.gut.blog.model.Post;
import pl.wsti.gut.blog.model.User;
import pl.wsti.gut.blog.service.CommentService;
import pl.wsti.gut.blog.service.PostService;

@RestController
public class CommentController {

	@Autowired
	PostService postService;
	@Autowired
	CommentService commentService;

	@RequestMapping(value = "/comments", method = RequestMethod.GET)
	public List<Comment> showCommentsForPost(HttpSession session,
			HttpServletRequest request) {

		// Post post = postService.getPostById(Long.parseLong(request
		// .getParameter("postId")));
		List<Comment> commentsPerPost = commentService
				.getCommentsForPostReversed(Long.parseLong(request
						.getParameter("postId")));
		// List<Comment> commentsPerPost2 = new ArrayList<>();
		// for (Comment c : commentsPerPost) {
		// commentsPerPost2.add(c);
		// System.out.println(c.getContent());
		// }
		return commentsPerPost;
	}

	@RequestMapping(value = "/comments", method = RequestMethod.POST)
	public @ResponseBody List<Comment> addComment(@RequestBody Comment comment,
			HttpServletRequest request, HttpServletResponse response) {
		long postId = Long.parseLong(request.getParameter("postId"));
		Post post = postService.getPostById(postId);
		comment.setPost(post);
		comment.setUser((User) request.getSession().getAttribute("user"));
		commentService.saveComment(comment);
		return commentService.getCommentsForPostReversed(postId);
	}
}
