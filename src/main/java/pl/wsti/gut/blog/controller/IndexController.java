package pl.wsti.gut.blog.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.wsti.gut.blog.model.Category;
import pl.wsti.gut.blog.model.Post;
import pl.wsti.gut.blog.model.User;
import pl.wsti.gut.blog.model.User.Role;
import pl.wsti.gut.blog.service.CategoryService;
import pl.wsti.gut.blog.service.PostService;

@Controller
public class IndexController {

	private final static int POSTS_ON_PAGE = 2;

	@Autowired
	PostService postService;
	@Autowired
	CategoryService categoryService;

	User user;

	@RequestMapping(value = "/index")
	public String showIndex(Model model, HttpServletRequest request,
			HttpSession session) {

		user = (User) session.getAttribute("user");
		if (user!=null && user.getId()==1){
			user.setRole(Role.ADMIN);
		}
		String page = request.getParameter("page");
		String categoryId = request.getParameter("categoryId");
		if ("".equals(categoryId)) {
			categoryId = null;
		}
		int pageNo;
		if (page == null) {
			return "forward:index.html?page=1"
					+ (categoryId != null ? "&categoryId=" + categoryId : "");
		} else {
			pageNo = Integer.valueOf(page);
		}
		List<Post> posts;
		if (categoryId == null) {
			posts = postService.getAllPostsReversed();
		} else {
			posts = postService.getAllPostsReversedPerCategoryId(Long
					.parseLong(categoryId));
		}
		int beg, end;
		if (pageNo <= 1) {
			beg = 0;
		} else if (POSTS_ON_PAGE * pageNo <= posts.size()) {
			beg = POSTS_ON_PAGE * (pageNo - 1);
		} else {
			beg = posts.size() - posts.size() % POSTS_ON_PAGE;
		}
		end = beg + POSTS_ON_PAGE;
		if (end >= posts.size()) {
			end = posts.size();
			model.addAttribute("last", true);
		}
		model.addAttribute("posts", posts.subList(beg, end));

		List<Category> categories = categoryService.findAll();
		model.addAttribute("categories", categories);

		return "index";
	}

	@RequestMapping(value = "/logout")
	public String logout(HttpSession session) {
		session.setAttribute("user", null);
		return "redirect:index.html";
	}

	// Random random = new Random(System.currentTimeMillis());
	// int count = Math.abs(random.nextInt() % 9 + 1);
	// String dynamicText = "Ilość powtórzeń to: " + count + "\n";
	// for (int i = 0; i < count; i++) {
	// dynamicText +=
	// "Uzupełnij swojego bloga o dynamicznie generowaną treść.";
	// }
	// model.addAttribute("dynamicText", dynamicText);

}
