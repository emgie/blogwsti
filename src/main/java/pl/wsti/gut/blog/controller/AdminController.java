package pl.wsti.gut.blog.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.wsti.gut.blog.model.User;
import pl.wsti.gut.blog.model.User.Role;
import pl.wsti.gut.blog.service.UserService;

@Controller
public class AdminController {

	@Autowired
	UserService userService;

	User user;

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String showAdminPanel(Model model, HttpServletRequest request,
			HttpSession session) {

		user = (User) session.getAttribute("user");
		if (user == null || user.getRole() != Role.ADMIN) {
			return "redirect:index.html";
		}

//		model.addAttribute("email", new Email());
		return "admin";
	}






}
