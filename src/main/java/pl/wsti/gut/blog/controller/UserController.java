package pl.wsti.gut.blog.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.wsti.gut.blog.model.Email;
import pl.wsti.gut.blog.model.EmailSender;
import pl.wsti.gut.blog.model.User;
import pl.wsti.gut.blog.model.User.Role;
import pl.wsti.gut.blog.service.UserService;
import pl.wsti.gut.blog.view.UserValidator;

@Controller
public class UserController {

	@Autowired
	UserService userService;
	@Autowired
	Email email;
	@Autowired
	UserValidator userValidator;
	User user;

	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public String showUserForm(Model model, HttpSession session) {

		// user = (User) session.getAttribute("user");
		// if (user == null || user.getRole() != Role.ADMIN) {
		// return "redirect:index.html";
		// }
		User newUser = (User) session.getAttribute("newUser");
		if (newUser == null) {
			newUser = new User();
		}
		model.addAttribute("newUser", newUser);

		return "user";
	}

	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public String addUser(@Valid @ModelAttribute("newUser") User newUser,
			BindingResult result, HttpServletRequest request) {

		userValidator.validate(newUser, result);
		if (result.hasErrors()) {
			return "user";
		} else {
			userService.addUser(newUser);
			email.setRecipient(newUser);
			email.setSubject("Witaj na blogu WSTI");
			email.setContent(String.format(
					"Twój nick: %1s\n\nPozdrawiam,\nMichał Gut",
					newUser.getNick()));
			email.send();
			String referrer = request.getHeader("referer");
			if (referrer.contains("admin")) {
				return "redirect:admin.html";
			}
			return "redirect:index.html";
		}
	}

}
