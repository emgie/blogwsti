package pl.wsti.gut.blog.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pl.wsti.gut.blog.model.Category;
import pl.wsti.gut.blog.model.Email;
import pl.wsti.gut.blog.model.EmailSender;
import pl.wsti.gut.blog.model.User;
import pl.wsti.gut.blog.service.CategoryService;
import pl.wsti.gut.blog.service.UserService;

@RestController
public class AdminRestController {

	@Autowired
	CategoryService categoryService;
	@Autowired
	UserService userService;
	@Autowired
	private EmailSender emailSender;

	@RequestMapping(value = "/admin/getCategories", method = RequestMethod.GET)
	public List<Category> getCategories() {
		return categoryService.findAll();
	}

	@RequestMapping(value = "/admin/deleteCategory", method = RequestMethod.POST)
	public @ResponseBody List<Category> deleteCategory(
			@RequestBody Category category) {
		categoryService.deleteCategory(category);
		return categoryService.findAll();
	}

	@RequestMapping(value = "/admin/addCategory", method = RequestMethod.POST)
	public @ResponseBody List<Category> addCategory(
			@RequestBody Category category) {
		categoryService.createCategory(category);
		return categoryService.findAll();
	}

	@RequestMapping(value = "/admin/email", method = RequestMethod.POST)
	public String sendEmail(@Valid @RequestBody Email email,
			BindingResult result) {
		if (!result.hasErrors())
			email.setEmailSender(emailSender);
		email.send();
		return null;
	}

	@RequestMapping(value = "/admin/getUsers", method = RequestMethod.GET)
	public List<User> getUsers(HttpSession session) {
		User user = (User) session.getAttribute("user");
		return userService.getAllUsersExceptOne(user);
	}

	@RequestMapping(value = "/admin/deleteUsers", method = RequestMethod.POST)
	public String deleteUsers(@RequestBody List<User> users) {

		for (User user : users) {
			userService.deleteUser(user);
		}
		return null;
	}

}
