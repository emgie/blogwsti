package pl.wsti.gut.blog.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.wsti.gut.blog.model.Category;
import pl.wsti.gut.blog.model.Post;
import pl.wsti.gut.blog.model.User;
import pl.wsti.gut.blog.service.CategoryService;
import pl.wsti.gut.blog.service.PostService;

@Controller
public class PostController {

	@Autowired
	PostService postService;
	
	@Autowired
	CategoryService categoryService;
	
	User user;
	
	@RequestMapping(value = "/post", method = RequestMethod.GET)
	public String showPostForm(Model model, HttpSession session) {
		
		user = (User) session.getAttribute("user");
		if (user==null){
			return "redirect:index.html";
		}
		Post post = (Post) session.getAttribute("post");
		if (post == null) {
			post = new Post();
		}
		model.addAttribute("post", post);
		List<Category> categories = categoryService.findAll();
		model.addAttribute("categories", categories);

		return "post";
	}

	@RequestMapping(value = "/post", method = RequestMethod.POST)
	public String addPost(@Valid Post post, BindingResult result) {
		if (result.hasErrors()) {
			return "post";
		} else {
			post.setUser(user);
			postService.addPost(post);
		}
		return "redirect:index.html";
	}
	
	@RequestMapping(value = "/deletePost", method = RequestMethod.POST)
	public String deletePost(Post post) {
		
		//TODO
		postService.deletePost(post);
		return "index.html";
	}
}
