package pl.wsti.gut.blog.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.wsti.gut.blog.model.User;
import pl.wsti.gut.blog.model.UserTemplate;
import pl.wsti.gut.blog.service.UserService;
import pl.wsti.gut.blog.view.UserValidator;

@Controller
public class LoginController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showLoginWindow(Model model, HttpSession session) {

		UserTemplate user = (UserTemplate) session.getAttribute("userTemplate");
		if (user == null) {
			user = new UserTemplate();
		} else {
			user.setPassword("");
		}
		model.addAttribute("userTemplate", user);
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String verifyUser(
			@Valid @ModelAttribute("userTemplate") UserTemplate userTemplate,
			BindingResult result, HttpSession session) {

		User user = userService.findUserIfExists(userTemplate);
		if (user == null || result.hasErrors()) {
			ObjectError error = new ObjectError("login", "Zły nick lub hasło!");
			result.addError(error);
			return "login";
		} else {
			session.setAttribute("user", user);
			return "redirect:index.html";
		}

	}

}
