package pl.wsti.gut.blog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.neo4j.cypher.internal.compiler.v2_1.planner.logical.steps.uniqueIndexSeekLeafPlanner;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import pl.wsti.gut.blog.common.Utility;

@Entity
@Component("user")
public class User {

	public enum Role {
		USER, ADMIN;
	}

	@NotEmpty
	@Column(unique = true)
	private String nick;
	@NotEmpty
	private String password;
	@NotEmpty
	@Transient
	private String passwordConfirm;
	@NotEmpty
	@Email
	@Column(unique = true)
	private String email;
	private String ggNo;
	private String wwwAddress;
	@Id
	@GeneratedValue
	private Long id;
	@Enumerated(EnumType.STRING)
	private Role role;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGgNo() {
		return ggNo;
	}

	public void setGgNo(String ggNo) {
		this.ggNo = ggNo;
	}

	public String getWwwAddress() {
		return wwwAddress;
	}

	public void setWwwAddress(String wwwAddress) {
		this.wwwAddress = wwwAddress;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = Utility.hashWithMD5(password);
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = Utility.hashWithMD5(passwordConfirm);
	}

	@PrePersist
	public void prePersist() {
		if (getRole() == null)
			setRole(Role.USER);
	}

}
