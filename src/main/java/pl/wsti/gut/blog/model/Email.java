package pl.wsti.gut.blog.model;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Email {

	@Autowired
	private EmailSender emailSender;

	private String subject;
	private String content;
	private List<User> recipients = new ArrayList<>();

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<User> getRecipients() {
		return recipients;
	}

	public void setRecipient(User user) {
		getRecipients().clear();
		getRecipients().add(user);
	}

	public void send() {
		try {
			getEmailSender().send(this);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public void setRecipients(List<User> recipients) {
		this.recipients = recipients;
	}

	public EmailSender getEmailSender() {
		return emailSender;
	}
	
	public void setEmailSender(EmailSender emailSender) {
		this.emailSender = emailSender;
	}

}
