package pl.wsti.gut.blog.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EmailSender {

	private String username;
	private String password;

	@Autowired
	public EmailSender(@Value("${email.user}") String username,
			@Value("${email.password}") String password) {
		this.username = username;
		this.password = password;
	}

	public void send(Email email) throws AddressException, MessagingException {

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});

		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(username + "@gmail.com"));
		List<String> recepients = new ArrayList<>();
		for (User user : email.getRecipients()) {
			recepients.add(user.getEmail());
		}
		message.setRecipients(Message.RecipientType.TO, InternetAddress
				.parse(StringUtils.join(recepients.toArray(), ",")));
		// message.setRecipient(Message.RecipientType.TO, new InternetAddress(
		// "michalgut1@gmail.com"));
		message.setSubject(email.getSubject());
		message.setText(email.getContent());
		Transport.send(message);

	}

}
