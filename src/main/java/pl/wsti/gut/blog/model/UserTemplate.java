package pl.wsti.gut.blog.model;

import org.hibernate.validator.constraints.NotEmpty;

import pl.wsti.gut.blog.common.Utility;

public class UserTemplate {

	@NotEmpty
	private String nick;
	@NotEmpty
	private String password;

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getPassword() {
		return Utility.hashWithMD5(password);
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
