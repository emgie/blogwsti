package pl.wsti.gut.blog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pl.wsti.gut.blog.model.User;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long> {

	User findByNickAndPassword(String nick, String password);

	User findUserByNick(String nick);

	User findUserByEmail(String email);

	@Query("SELECT u FROM User u WHERE u.id <> ?1")
	List<User> findAllUsersExceptOne(Long userId);

}
