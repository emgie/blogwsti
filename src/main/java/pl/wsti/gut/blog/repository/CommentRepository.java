package pl.wsti.gut.blog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pl.wsti.gut.blog.model.Comment;

@Repository("commentRepository")
public interface CommentRepository extends JpaRepository<Comment, Long> {

	@Query("Select c from Comment c where c.post.id = ?1")
	List<Comment> findAllCommentsByPost(long postId);

}
