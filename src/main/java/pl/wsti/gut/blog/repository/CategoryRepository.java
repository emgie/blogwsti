package pl.wsti.gut.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.wsti.gut.blog.model.Category;

@Repository("categoryRepository") 
public interface CategoryRepository extends JpaRepository<Category, Long> {

}
