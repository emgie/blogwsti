package pl.wsti.gut.blog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.wsti.gut.blog.model.Post;

@Repository("postRepository")
public interface PostRepository extends JpaRepository<Post, Long> {

	List<Post> findByCategoryIdOrderByIdDesc(long categoryId);



}
