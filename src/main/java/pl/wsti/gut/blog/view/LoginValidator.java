package pl.wsti.gut.blog.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import pl.wsti.gut.blog.model.UserTemplate;
import pl.wsti.gut.blog.service.UserService;

public class LoginValidator implements Validator {

	@Autowired
	UserService userService;

	@Override
	public void validate(Object target, Errors error) {
		UserTemplate user = (UserTemplate) target;
		if (userService.findUserIfExists(user) == null) {
			error.rejectValue("coś", "coś innego");
		}
	}

	@Override
	public boolean supports(Class<?> arg0) {
		return UserTemplate.class.equals(arg0);
	}

}