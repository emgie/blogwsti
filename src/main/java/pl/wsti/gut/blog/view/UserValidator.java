package pl.wsti.gut.blog.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import pl.wsti.gut.blog.model.User;
import pl.wsti.gut.blog.service.UserService;

@Component
public class UserValidator implements Validator {

	@Autowired
	UserService userService;
	
	@Override
	public void validate(Object target, Errors error) {
		User user = (User) target;
		if (userService.findUserByNick(user.getNick())!=null){
			error.rejectValue("nick", "nick.unique");
		}
		if (userService.findUserByEmail(user.getEmail())!=null){
			error.rejectValue("email", "email.unique");
		}
		if (!user.getPassword().equals(user.getPasswordConfirm())) {
			error.rejectValue("password", "password.confirm");
		}
	}

	@Override
	public boolean supports(Class<?> arg0) {
		return User.class.equals(arg0);
	}

}