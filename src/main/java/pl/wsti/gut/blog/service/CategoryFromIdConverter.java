package pl.wsti.gut.blog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import pl.wsti.gut.blog.model.Category;

public class CategoryFromIdConverter implements Converter<String, Category> {

	    @Autowired
	    CategoryService categoryService;

	    public Category convert(String id) {     
	        return categoryService.findById(Integer.parseInt(id));
	    }
	}
