package pl.wsti.gut.blog.service;

import java.util.List;

import pl.wsti.gut.blog.model.Comment;

public interface CommentService {

	void saveComment(Comment comment);

	public List<Comment> getCommentsForPost(long postId);

	public List<Comment> getCommentsForPostReversed(long parseLong);

}
