package pl.wsti.gut.blog.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.wsti.gut.blog.model.Category;
import pl.wsti.gut.blog.repository.CategoryRepository;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	CategoryRepository categoryRepository;

	@Override
	public void createCategory(Category category) {
		categoryRepository.save(category);
	}

	@Override
	public void deleteCategory(Category category) {
		category = categoryRepository.getOne(category.getId());
		categoryRepository.delete(category);
	}

	@Override
	public List<Category> findAll() {
		return categoryRepository.findAll();
	}

	@Override
	public Category findById(long id) {
		return categoryRepository.findOne(id);
	}

}
