package pl.wsti.gut.blog.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.wsti.gut.blog.model.User;
import pl.wsti.gut.blog.model.UserTemplate;
import pl.wsti.gut.blog.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Transactional
	@Override
	public void addUser(User user) {
		userRepository.save(user);
	}

	@Override
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public User findUserIfExists(UserTemplate userTemplate) {
		return userRepository.findByNickAndPassword(userTemplate.getNick(),
				userTemplate.getPassword());
	}

	@Override
	public User findUserIfExists(User user) {
		return userRepository.findByNickAndPassword(user.getNick(),
				user.getPassword());
	}

	@Transactional
	@Override
	public void deleteUser(User user) {
		userRepository.delete(user);
	}

	@Override
	public User findUserByNick(String nick) {
		return userRepository.findUserByNick(nick);
	}

	@Override
	public User findUserByEmail(String email) {
		return userRepository.findUserByEmail(email);
	}

	@Override
	public List<User> getAllUsersExceptOne(User user) {
		return userRepository.findAllUsersExceptOne(user.getId());
	}

}
