package pl.wsti.gut.blog.service;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.wsti.gut.blog.model.Comment;
import pl.wsti.gut.blog.model.Post;
import pl.wsti.gut.blog.repository.CommentRepository;
import pl.wsti.gut.blog.repository.PostRepository;

@Service
public class PostServiceImpl implements PostService {

	@Autowired
	PostRepository postRepository;

	@Override
	public List<Post> getAllPosts() {
		return postRepository.findAll();
	}

	@Override
	@Transactional
	public void addPost(Post post) {
		postRepository.saveAndFlush(post);
	}

	@Override
	public List<Post> getAllPostsReversed() {
		List<Post> posts = getAllPosts();
		Collections.reverse(posts);
		return posts;
	}

	@Override
	public Post getPostById(long id) {
		return postRepository.findOne(id);
	}

	@Override
	@Transactional
	public void updatePost(Post post) {
		postRepository.saveAndFlush(post);
	}

	@Override
	@Transactional
	public void deletePost(Post post) {
		postRepository.delete(post);

	}

	@Override
	public List<Post> getAllPostsReversedPerCategoryId(long categoryId) {
		return postRepository.findByCategoryIdOrderByIdDesc(categoryId);
	}

}
