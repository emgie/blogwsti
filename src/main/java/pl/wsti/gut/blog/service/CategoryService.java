package pl.wsti.gut.blog.service;

import java.util.List;

import pl.wsti.gut.blog.model.Category;

public interface CategoryService {

	public void createCategory(Category category);

	public void deleteCategory(Category category);

	public List<Category> findAll();

	public Category findById(long id);

}
