package pl.wsti.gut.blog.service;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.wsti.gut.blog.model.Comment;
import pl.wsti.gut.blog.repository.CommentRepository;

@Service
public class CommentServiceImpl implements CommentService {

	@Autowired
	CommentRepository commentRepository;

	@Override
	@Transactional
	public void saveComment(Comment comment) {
		commentRepository.saveAndFlush(comment);
	}

	@Override
	@Transactional
	public List<Comment> getCommentsForPost(long postId) {
	
		return commentRepository.findAllCommentsByPost(postId);
	}
	
	

	@Override
	@Transactional
	public List<Comment> getCommentsForPostReversed(long postId) {
		List<Comment> comments = getCommentsForPost(postId);
		Collections.reverse(comments);
		return comments;
	}
	
}
