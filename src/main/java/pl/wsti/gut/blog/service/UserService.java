package pl.wsti.gut.blog.service;

import java.util.List;

import pl.wsti.gut.blog.model.User;
import pl.wsti.gut.blog.model.UserTemplate;

public interface UserService {

	public List<User> getAllUsers();

	public void addUser(User user);

	public User findUserIfExists(UserTemplate userTemplate);

	public User findUserIfExists(User user);

	public void deleteUser(User user);

	public User findUserByNick(String nick);

	public User findUserByEmail(String nick);

	public List<User> getAllUsersExceptOne(User user);


}
