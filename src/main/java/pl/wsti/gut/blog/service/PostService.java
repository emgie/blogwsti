package pl.wsti.gut.blog.service;

import java.util.List;

import pl.wsti.gut.blog.model.Post;

public interface PostService {

	public List<Post> getAllPosts();

	public List<Post> getAllPostsReversed();

	public List<Post> getAllPostsReversedPerCategoryId(long categoryId);

	public void addPost(Post post);

	public Post getPostById(long id);

	public void updatePost(Post post);

	public void deletePost(Post post);

}
