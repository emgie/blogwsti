package pl.wsti.gut.blog.common;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.lang3.StringUtils;

public class Utility {

	public static String hashWithMD5(String text) {
		if (text == null || text.isEmpty()) {
			return "";
		}
		try {
			byte[] bytes = MessageDigest.getInstance("MD5").digest(
					text.getBytes());
			BigInteger bigInt = new BigInteger(1, bytes);
			String hashtext = bigInt.toString(16);
			return StringUtils.leftPad(hashtext, 32, '0');
		} catch (NoSuchAlgorithmException e) {
			return "";
		}
	}

}
