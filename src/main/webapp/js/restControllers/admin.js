function CategoryController($scope, $http) {
	$scope.updateData = function() {
		$http.get('admin/getCategories.json').success(function(data) {
			$scope.categories = data;
		});
	}
	$scope.updateData();
	$scope.category = {};
	$scope.addCategory = function(category) {
		$http({
			method : 'POST',
			url : 'admin/addCategory.json',
			data : category
		}).success(function(data) {
			$scope.categories = data;
		});
		$scope.category.name = "";
	};
	$scope.deleteCategory = function(category) {
		$http({
			method : 'POST',
			url : 'admin/deleteCategory.json',
			data : category
		}).success(function(data) {
			$scope.categories = data;
		});
		;
		// $scope.comments.push($scope.comment)
		// $scope.comment.content = "";
		// $scope.updateData(postId);

	};
}

function UserController($scope, $http) {
	$scope.email = {};
	$scope.email.recipients = [];
	$scope.usersToDelete = [];
	$scope.updateData = function() {
		$http.get('admin/getUsers.json').success(function(data) {
			$scope.users = data;
		});
	}
	$scope.updateData();
	$scope.userGroups = [ {
		text : 'dowolna'
	}, 'wszyscy', 'żaden', 'użytkownicy', 'administratorzy' ];
	$scope.addUserToEmail = function(user, value) {
		if (value) {
			$scope.email.recipients.push(user);
		} else {
			$scope.email.recipients.splice($scope.email.recipients
					.indexOf(user), 1);
		}
	};
	$scope.send = function(email) {
		$http({
			method : 'POST',
			url : 'admin/email.json',
			data : $scope.email
		}).success(function() {
			$scope.email.subject = "";
			$scope.email.content = "";
		});
	};
	$scope.addUserToUsersToDelete = function(user, value) {
		if (value) {
			$scope.usersToDelete.push(user);
		} else {
			$scope.usersToDelete.splice($scope.usersToDelete.indexOf(user), 1);
		}
	};
	$scope.deleteUsers = function() {
		$http({
			method : 'POST',
			url : 'admin/deleteUsers.json',
			data : $scope.usersToDelete
		}).success(function() {
			$scope.usersToDelete = [];
			$scope.updateData();
		});
	};
}