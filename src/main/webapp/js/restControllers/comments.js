//function Comments($scope, $http) {
//	$http.get('comments.json?postId=' + $scope.postId).success(function(data) {
//		$scope.comments = data;
//	});
//}

function CommentController($scope, $http) {
	$scope.updateData = function(postId) {
		$http.get('comments.json?postId=' + postId).success(function(data) {
			$scope.comments = data;
		});
	}
	$scope.updateData($scope.postId);
	$scope.comment = {};
	$scope.add = function(postId) {
		$http({
			method : 'POST',
			url : 'comments.json?postId=' + postId,
			data : $scope.comment
		}).success(function(data) {
			$scope.comments = data;
		});
		;
		$scope.comments.push($scope.comment)
		$scope.comment.content = "";
		// $scope.updateData(postId);
	};
}