<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html class="no-js" lang="pl"
	data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Blog WSTI</title>
<%@ include file="/WEB-INF/jsp/items/scirptsAndStyles.jsp"%>
</head>
<body>
	<div class="row large-6 columns small-centered">
		<div class="panel" role="content">
			<h1>Zaloguj się</h1>
			<form:form commandName="userTemplate">
				<form:errors path="${login}" cssClass="errorblock" element="div" />
				<label>Login:</label>
				<form:input path="nick" cssErrorClass="error" />
				<form:errors path="nick" cssClass="error" />
				<label>Hasło:</label>
				<form:password path="password" cssErrorClass="error" />
				<form:errors path="password" cssClass="error" />
				<input type="submit" class="button" value="Zaloguj" />
				<br>
			</form:form>
			<a href="user.html">Nie masz konta? Zarejestruj się</a>
		</div>
		<br> <br> <a href="index.html">Powrót do strony głównej</a>
	</div>
	<p></p>
	<%@ include file="/WEB-INF/jsp/items/footer.jsp"%>
</body>
</html>
