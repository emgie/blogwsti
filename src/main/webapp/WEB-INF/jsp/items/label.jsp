<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="row">
	<div class="large-12 columns">
		<div class="nav-bar right">
			<ul class="button-group">
				<c:choose>
					<c:when test="${empty sessionScope.user}">
						<li><a href="login.html" class="button">Zaloguj się</a></li>
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${fn:contains(pageContext.request.requestURI,'admin')}">
								<li><a href="index.html" class="button">Strona główna</a></li>
							</c:when>
							<c:otherwise>
								<c:if test="${sessionScope.user.role eq 'ADMIN'}">
									<li><a href="admin.html" class="button">Panel
											administratora</a></li>
								</c:if>
							</c:otherwise>
						</c:choose>
						<li><a href="post.html" class="button">Dodaj post</a></li>
						<li><a href="logout.html" class="button">Wyloguj się</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>
		<h1>
			Blog WSTI <small>Autor: Michał Gut </small>
			<!-- 				Blog WSTI <small>Aplikacja na Systemy Sieciowe.</small> -->
		</h1>
		<img src="img/panorama.jpg">
		<hr />
	</div>
</div>

