<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!doctype html>
<html ng-app class="no-js" lang="pl"
	data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Blog WSTI</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.8/angular.min.js"></script>
<%@ include file="/WEB-INF/jsp/items/scirptsAndStyles.jsp"%>
<script src="js/restControllers/admin.js"></script>
</head>
<body>

	<%@ include file="/WEB-INF/jsp/items/label.jsp"%>

	<div class="row">
		<div class="large-8 columns">
			<h3>Użytkownicy</h3>
		</div>
	</div>
	<div class="row" ng-controller="UserController">
		<div class="large-8 columns">
			<table style="width: 100%">
				<!-- 				<tr>
					<th columnspan="2">Nick</th>
					<th columnspan="2">e-mail</th>
					<th><div style="text-align: center;">
							<img src="img/email.png" height="42" width="42">
						</div> <select ng-options="item in userGroups"></select></th>
					<th>
						<div style="text-align: center;">
							<img src="img/delete-user.jpg" height="42" width="42">
						</div> <select></select>
					</th>
				</tr> -->
				<tr>
					<th>Nick</th>
					<th>e-mail</th>
					<th><img src="img/email.png" height="36" width="36"></th>
					<th><img src="img/delete-user.jpg" height="36" width="36">
					</th>
				</tr>
				<tr ng-repeat="user in users">
					<td>{{user.nick}}</td>
					<td>{{user.email}}</td>
					<td><input type="checkbox" ng-model="addUserValue"
						ng-change="addUserToEmail(user,addUserValue)" /></td>
					<td><input type="checkbox" ng-model="deleteUserValue"
						ng-change="addUserToUsersToDelete(user,deleteUserValue)"></td>
				</tr>
				<tr>
					<td rowspan="1">
						<button class="button" ng-click="deleteUsers()">Usuń
							wybranych uzytkowników</button>
					</td>
				</tr>
			</table>
		</div>
		<div class="large-4 columns panel">
			<h6>Wyślij wiadomość</h6>
			<div>
				<label>Tytuł:</label> <input ng-model="email.subject" type="text" />
				<label>Treść:</label>
				<textarea rows="7" ng-model="email.content"></textarea>
				<button class="button" ng-click="send(email)">Wyślij</button>
				<br>
			</div>
		</div>
		<div class="large-4 columns panel">
			<h4>
				<a href="user.html" class="button ">Dodaj nowego użytkownika</a>
			</h4>
		</div>
	</div>
	<div class="row">
		<hr>
		<div ng-controller="CategoryController" class="large-12 columns">
			<h3>Kategorie</h3>
			<div class="row collapse">
				<div class="large-8 small-9 columns">
					<input ng-model="category.name" type="text"
						placeholder="Dodaj kategorię" />
				</div>
				<div class="large-4 small-3 columns ">
					<a ng-click="addCategory(category)" class="button ">Dodaj</a>
				</div>
			</div>

			<div class="row collapse" ng-repeat="category in categories">
				<div class="large-8 small-9 columns">
					<label class="panel">{{category.name}}</label>
				</div>
				<div class="large-4 small-3 columns ">
					<button ng-click="deleteCategory(category)">Usuń</button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<br> <br>
		<p />
		<a href="index.html">Powrót do strony głównej</a>
	</div>
	<%@ include file="/WEB-INF/jsp/items/footer.jsp"%>
</body>
</html>