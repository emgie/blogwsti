<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!doctype html>
<html ng-app class="no-js" lang="pl"
	data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Blog WSTI</title>
<%@ include file="/WEB-INF/jsp/items/scirptsAndStyles.jsp"%>
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.8/angular.min.js"></script>
<script src="js/restControllers/comments.js"></script>
</head>
<body>
	<%@ include file="/WEB-INF/jsp/items/label.jsp"%>
	<div class="row">
		<div class="large-9 columns" role="content">
			<c:forEach items="${posts}" var="post" varStatus="postLoop">
				<article ng-init="postId = ${post.id};">

					<h3>
						<a href="#"><c:out value="${post.title}" /></a>
					</h3>
					<h6>
						Dodane przez <a href="#">${post.user.nick}</a> w dniu
						${post.localDate}
					</h6>
					<h6>
						Kategoria: <a href="#">${post.category.name}</a>
					</h6>
					<div class="row">
						<div class="large-12 columns">
							<p>${post.content}</p>
						</div>
						<div class="large-6 columns">
							<c:forEach begin="0" end="9" varStatus="loop">
								<%-- <img src="img/0${loop.index}.jpg" /> --%>
								<%-- <img src="img/0${loop.index}.jpg" height="64" width="64" /> --%>
							</c:forEach>
						</div>
					</div>
					<br>
					<div class="panel" ng-controller="CommentController">
						<h4>Komentarze</h4>
						<c:choose>
							<c:when test="${!empty sessionScope.user}">
								<form novalidate>
									Dodaj komentarz:<br>
									<textarea rows="5" ng-model="comment.content"></textarea>
									<br>
									<button ng-click="add(${post.id})">Dodaj</button>
								</form>
							</c:when>
						</c:choose>
						<div class="comments-container">
							<div ng-repeat="comment in comments">
								<h6 class="subheader">
									Dodane przez <a href="#">{{comment.user.nick}}</a> w dniu
									{{comment.localDateString}}.
								</h6>
								<p>{{comment.content}}</p>
								<hr ng-hide="$last">
							</div>
						</div>
						<!-- <a href="#">Read More &rarr;</a> -->
					</div>
				</article>
				<c:if test="${fn:length(posts)-1>postLoop.index}">

					<hr />
				</c:if>
			</c:forEach>
			<!-- 			<article>
				<h3>
					<a href="#">Blog Post Title</a>
				</h3>
				<h6>
					Dodane przez <a href="#">Michał Gut</a> w dniu 01-09-2015.
				</h6>
				<div class="row">
					<div class="large-6 columns">
						<p>Bacon ipsum dolor sit amet nulla ham qui sint exercitation
							eiusmod commodo, chuck duis velit. Aute in reprehenderit, dolore
							aliqua non est magna in labore pig pork biltong. Eiusmod swine
							spare ribs reprehenderit culpa.</p>
						<p>Boudin aliqua adipisicing rump corned beef. Nulla corned
							beef sunt ball tip, qui bresaola enim jowl. Capicola short ribs
							minim salami nulla nostrud pastrami.</p>
					</div>
					<div class="large-6 columns">
						<img src="http://placehold.it/400x240&text=[img]" />
					</div>
				</div>
				<p>Pork drumstick turkey fugiat. Tri-tip elit turducken pork
					chop in. Swine short ribs meatball irure bacon nulla pork belly
					cupidatat meatloaf cow. Nulla corned beef sunt ball tip, qui
					bresaola enim jowl. Capicola short ribs minim salami nulla nostrud
					pastrami. Nulla corned beef sunt ball tip, qui bresaola enim jowl.
					Capicola short ribs minim salami nulla nostrud pastrami.</p>
				<p>Pork drumstick turkey fugiat. Tri-tip elit turducken pork
					chop in. Swine short ribs meatball irure bacon nulla pork belly
					cupidatat meatloaf cow. Nulla corned beef sunt ball tip, qui
					bresaola enim jowl. Capicola short ribs minim salami nulla nostrud
					pastrami.</p>
			</article>-->
		</div>


		<aside class="large-3 columns">
			<div class="panel">
				<h5>Kategorie</h5>
				<ul class="side-nav">
					<li><a href="index.html?page=1">Wszystkie</a></li>
					<c:forEach var="category" items="${categories}">
						<li><a href="index.html?categoryId=${category.id}">${category.name}</a></li>
					</c:forEach>
				</ul>
			</div>
		</aside>

	</div>


	<footer class="row">
		<div class="large-12 columns">
			<div class="row">
				<div class="large-6 columns">
					<ul class="inline-list right">
						<li><c:if test="${param.page>1}">
								<a href="index.html?page=${param.page-1}">Poprzednia strona</a>
							</c:if></li>
						<li><c:choose>
								<c:when test="${!last}">
									<a
										href="index.html?page=${param.page+1}&categoryId=${param.categoryId}">Następna
										strona</a>
								</c:when>
								<c:otherwise>
									<a style="display: none;"
										href="index.html?page=${param.page+1}&categoryId=${param.categoryId}">Następna
										strona</a>
								</c:otherwise>
							</c:choose></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<%@ include file="/WEB-INF/jsp/items/footer.jsp"%>
</body>
</html>