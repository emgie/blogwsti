<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html class="no-js" lang="pl"
	data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Blog WSTI</title>
<%@ include file="/WEB-INF/jsp/items/scirptsAndStyles.jsp"%>
</head>
<body>
	<div class="row large-6 columns small-centered" >
		<div role="content">
			<h1>Dodaj post</h1>
			<form:form commandName="post">
				<label>Tytuł:</label>
				<form:input path="title" cssErrorClass="error" />
				<form:errors path="title" cssClass="error" />
				<label>Kategoria:</label>
				<form:select path="category" cssErrorClass="error">
					<form:options items="${categories}" itemValue="id" itemLabel="name" />
				</form:select>
				<form:errors path="title" cssClass="error" />
				<label>Treść posta:</label>
				<form:textarea style="height: 250px;" path="content"
					cssErrorClass="error" />
				<form:errors path="content" cssClass="error" />
				<input type="submit" class="button" value="Dodaj post" />
				<br>
			</form:form>
			<br> <br> <a href="index.html">Powrót do strony głównej</a>
		</div>
	</div>
	<p></p>
	<%@ include file="/WEB-INF/jsp/items/footer.jsp"%>
</body>
</html>
