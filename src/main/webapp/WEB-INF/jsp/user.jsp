<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html class="no-js" lang="pl"
	data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Blog WSTI</title>
<%@ include file="/WEB-INF/jsp/items/scirptsAndStyles.jsp"%>
</head>
<body>
	<div class="row large-6 columns small-centered">
		<div role="content">
			<h1>Dodaj użytkownika</h1>
			<form:form commandName="newUser">
				<div class="panel">
					<label>Nick:</label>
					<form:input path="nick" cssErrorClass="error" />
					<form:errors path="nick" cssClass="error" />
					<label>Hasło:</label>
					<form:password path="password" cssErrorClass="error" />
					<form:errors path="password" cssClass="error" />
					<label>Potwierdź hasło:</label>
					<form:password path="passwordConfirm" cssErrorClass="error" />
					<form:errors path="passwordConfirm" cssClass="error" />
				</div>
				<div class="panel">
					<label>E-Mail:</label>
					<form:input path="email" cssErrorClass="error" />
					<form:errors path="email" cssClass="error" />
					<label>Numer GG:</label>
					<form:input path="ggNo" cssErrorClass="error" />
					<form:errors path="ggNo" cssClass="error" />
					<label>Adres WWW:</label>
					<form:input path="wwwAddress" cssErrorClass="error" />
					<form:errors path="wwwAddress" cssClass="error" />
					<c:if test="${sessionScope.user.role eq 'ADMIN'}">
						<label>Rola:</label>
						<form:select path="role">
							<form:option value="USER" label="User" />
							<form:option value="ADMIN" label="Admin" />
						</form:select>
					</c:if>
				</div>
				<input type="submit" class="button" value="Dodaj użytkownika" />
				<br>
			</form:form>
		</div>
		<br> <br> <a href="index.html">Powrót do strony głównej</a>
	</div>
	<p></p>
	<%@ include file="/WEB-INF/jsp/items/footer.jsp"%>
</body>
</html>
